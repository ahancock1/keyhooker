﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace KeyHooker
{
    public struct KBDLLHOOKSTRUCT
    {
        public int vkCode;
        public int scanCode;
        public int flags;
        public int time;
        public int dwExtraInfo;
    }

    public struct VirtualKeys
    {
        public const int TAB = 0x9;
        public const int CONTROL = 0x11;
        public const int ESCAPE = 0x1b;
        public const int DELETE = 0x2e;
    }

    public struct WindowsHook
    {
        // https://msdn.microsoft.com/en-us/library/windows/desktop/ms644990(v=vs.85).aspx
        public const int CALLWNDPROC = 4;
        public const int CALLWNDPROCRET = 12;
        public const int CBT = 5;
        public const int DEBUG = 9;
        public const int FOREGROUNDIDLE = 11;
        public const int GETMESSAGE = 3;
        public const int JOURNALPLAYBACK = 1;
        public const int JOURNALRECORD = 0;
        public const int KEYBOARD = 2;
        public const int KEYBOARD_LL = 13;
        public const int MOUSE = 7;
        public const int MOUSE_LL = 14;
        public const int MSGFILTER = -1;
        public const int SHELL = 10;
        public const int SYSMSGFILTER = 6;
    }

    public static class Keyboard
    {
        /// <summary>
        /// Call Unhook passing the return value from <see cref="Hook"/>
        /// to restore the original keyboard hook.
        /// </summary>
        /// <param name="hHook">Original hook address</param>
        /// <returns></returns>
        [DllImport("user32.dll",
            EntryPoint = "UnhookWindowsHookEx",
            CallingConvention = CallingConvention.StdCall)]
        private static extern int UnhookKeyboard(int hHook);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idHook"></param>
        /// <param name="lpfn"></param>
        /// <param name="hmod"></param>
        /// <param name="dwThreadId"></param>
        /// <returns>Address of replaced hook</returns>
        [DllImport("user32.dll", 
            EntryPoint = "SetWindowsHookExA")]
        private static extern int HookKeyboard(int idHook, KeyboardHookDelegate lpfn, IntPtr hmod, int dwThreadId);

        [DllImport("user32.dll",
            EntryPoint = "GetAsyncKeyState",
            CallingConvention = CallingConvention.StdCall)]
        private static extern int GetAsyncKeyState(int vKey);

        [DllImport("user32.dll",
            EntryPoint = "CallNextHookEx",
            CallingConvention = CallingConvention.StdCall)]
        private static extern int CallNextHookEx(int hHook, int nCode, int wParam, KBDLLHOOKSTRUCT lParam);
        

        // Keyboard Const
        private const int HC_ACTION = 0;
        private const int LLKHF_EXTENDED = 0x1;
        private const int LLKHF_INJECTED = 0x10;
        private const int LLKHF_ALTDOWN = 0x20;
        private const int LLKHF_UP = 0x80;
        
        public static int KeyboardHandle;


        private static bool IsHooked(ref KBDLLHOOKSTRUCT hookstruct)
        {
            Debug.WriteLine("Hookstruct.vkCode: {0}", hookstruct.vkCode);
            Debug.WriteLine(hookstruct.vkCode = VirtualKeys.ESCAPE);
            Debug.WriteLine(hookstruct.vkCode = VirtualKeys.TAB);

            if ((hookstruct.vkCode == VirtualKeys.ESCAPE) & Convert.ToBoolean(GetAsyncKeyState(VirtualKeys.CONTROL) & 0x8000))
            {
                // block these keys by return true
                return true;
            }

            return false;
        }
        
        public delegate int KeyboardHookDelegate(int code, int wParam, ref KBDLLHOOKSTRUCT lParam);

        [MarshalAs(UnmanagedType.FunctionPtr)]
        private static KeyboardHookDelegate _callback;

        public static int KeyboardCallback(int code, int wParam, ref KBDLLHOOKSTRUCT lParam)
        {

            if ((code == HC_ACTION))
            {
                Debug.WriteLine("Calling IsHooked");

                if ((IsHooked(ref lParam)))
                {
                    return 1;
                }
            }

            return CallNextHookEx(KeyboardHandle, code, wParam, lParam);
        }
        
        /// <summary>
        /// Returns true if the keyboard is hooked
        /// </summary>
        /// <returns></returns>
        public static bool IsHooked()
        {
            return KeyboardHandle != 0;
        }

        /// <summary>
        /// Hooks the keyboard
        /// </summary>
        public static void Hook()
        {
            int hookID = WindowsHook.KEYBOARD_LL;
            _callback = new KeyboardHookDelegate(Keyboard.KeyboardCallback);
            //int hmod = Marshal.GetHINSTANCE(Assembly.GetExecutingAssembly().GetModules()[0]).ToInt32();
            IntPtr hmod = Marshal.GetFunctionPointerForDelegate(_callback);

            KeyboardHandle = HookKeyboard(hookID, _callback, hmod, 0);

            if (IsHooked())
            {
                Debug.WriteLine("Keyboard hooked");
            }
            else
            {
                int hr = Marshal.GetLastWin32Error();
                Debug.WriteLine("keyboard hook failed: {0}", hr);
            }
        }
        
        /// <summary>
        /// Unhooks the keyboard and returns it back to its original hook
        /// </summary>
        public static void Unhook()
        {
            if (IsHooked())
            {
                UnhookKeyboard(KeyboardHandle);
            }
        }
    }
}
