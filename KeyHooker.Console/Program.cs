﻿using System;
using System.Timers;
using System.Windows.Forms;
using Hooker;

namespace KeyHooker.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            Keyboard Keyboard = new Keyboard();

            Keyboard.Hook();

            if (Keyboard.IsHooked())
            {
                System.Console.WriteLine("Hooked");
            }
            else
            {
                System.Console.WriteLine("Failed");
            }
            Application.Run();
            
            Keyboard.Unhook();
        }
    }
}
