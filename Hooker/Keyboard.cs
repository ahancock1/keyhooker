﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Hooker
{
    public struct KeyboardHook
    {
        public int vkCode;
        public int scanCode;
        public int flags;
        public int time;
        public int dwExtraInfo;
    }

    public class Keyboard : BaseHook
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto,
         CallingConvention = CallingConvention.StdCall)]
        private static extern int GetAsyncKeyState(int vKey);

        protected override bool OnHookInput(int nCode, IntPtr wParam, IntPtr lParam)
        {
           KeyboardHook args = (KeyboardHook)Marshal.PtrToStructure(lParam, typeof(KeyboardHook));


            return true;
        }
    }
}
