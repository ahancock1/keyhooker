﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Hooker
{
    public enum HookType 
    {
        CALLWNDPROC = 4,
        CALLWNDPROCRET = 12,
        CBT = 5,
        DEBUG = 9,
        FOREGROUNDIDLE = 11,
        GETMESSAGE = 3,
        JOURNALPLAYBACK = 1,
        JOURNALRECORD = 0,
        KEYBOARD = 2,
        KEYBOARD_LL = 13,
        MOUSE = 7,
        MOUSE_LL = 14,
        MSGFILTER = -1,
        SHELL = 10,
        SYSMSGFILTER = 6
    }

    public abstract class BaseHook
    {
        //public delegate int KeyboardHookDelegate(int code, int wParam, ref KBDLLHOOKSTRUCT lParam);
        
        public delegate int HookDelegate(int nCode, IntPtr wParam, IntPtr lParam);

        //This is the Import for the SetWindowsHookEx function.
        //Use this function to install a thread-specific hook.
        [DllImport("user32.dll", CharSet = CharSet.Auto,
         CallingConvention = CallingConvention.StdCall)]
        public static extern int SetWindowsHookEx(int idHook, HookDelegate lpfn,
        IntPtr hInstance, int threadId);

        //This is the Import for the UnhookWindowsHookEx function.
        //Call this function to uninstall the hook.
        [DllImport("user32.dll", CharSet = CharSet.Auto,
         CallingConvention = CallingConvention.StdCall)]
        public static extern bool UnhookWindowsHookEx(int idHook);

        //This is the Import for the CallNextHookEx function.
        //Use this function to pass the hook information to the next hook procedure in chain.
        [DllImport("user32.dll", CharSet = CharSet.Auto,
         CallingConvention = CallingConvention.StdCall)]
        public static extern int CallNextHookEx(int idHook, int nCode,
        IntPtr wParam, IntPtr lParam);


        [MarshalAs(UnmanagedType.FunctionPtr)]
        private HookDelegate callback;

        public int HookId = 0;
        public int HookHandle = 0;

        protected abstract bool OnHookInput(int nCode, IntPtr wParam, IntPtr lParam);

        private int HookCallback(int nCode, IntPtr wParam, IntPtr lParam)
        {
            Console.WriteLine("{0} {1} {2}", nCode, wParam, lParam);

            if (OnHookInput(nCode, wParam, lParam))
            {
                
                return CallNextHookEx(HookHandle, nCode, wParam, lParam);
            }
            return 1;

            
            //Marshall the data from the callback.
            //MouseHookStruct MyMouseHookStruct = (MouseHookStruct)Marshal.PtrToStructure(lParam, typeof(MouseHookStruct));

            //if (nCode < 0)
            //{
            //    return CallNextHookEx(hHook, nCode, wParam, lParam);
            //}
            //else
            //{
            //    //Create a string variable that shows the current mouse coordinates.
            //    String strCaption = "x = " +
            //            MyMouseHookStruct.pt.x.ToString("d") +
            //                "  y = " +
            //    MyMouseHookStruct.pt.y.ToString("d");
            //    //You must get the active form because it is a static function.
            //    Form tempForm = Form.ActiveForm;

            //    //Set the caption of the form.
            //    tempForm.Text = strCaption;
            //    return CallNextHookEx(HookHandle, nCode, wParam, lParam);
            //}


            return 1;
        }


        public virtual void Hook()
        {
            // Create an instance of HookProc.
            callback = new HookDelegate(HookCallback);
            int hmod = Marshal.GetHINSTANCE(Assembly.GetExecutingAssembly().GetModules()[0]).ToInt32();

            HookHandle = SetWindowsHookEx((int)HookType.KEYBOARD_LL,
                        callback,
                        (IntPtr)0,
                        0);

            if (IsHooked())
            {
                Console.WriteLine("Hook successful");
            }
            else
            {
                int hr = Marshal.GetLastWin32Error();
                Console.WriteLine("keyboard hook failed: {0}", hr);
            }
        }

        public bool IsHooked()
        {
            return HookHandle != 0;
        }

        public void Unhook()
        {
            bool ret = UnhookWindowsHookEx(HookHandle);

            if (ret == false)
            {
                Console.WriteLine("UnhookWindowsHookEx Failed");
            }
        }
    }
}
